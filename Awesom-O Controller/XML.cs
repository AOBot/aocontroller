﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Awesom_O_Controller
{
    class Xml
    {
        public static void Serialize(string filepath, Type type, object obj)
        {
            if (!Directory.Exists(filepath))
                Directory.CreateDirectory(filepath.Remove(filepath.LastIndexOf("\\")));
            try
            {
                using (TextWriter w = new StreamWriter(filepath))
                {
                    var s = new XmlSerializer(type);
                    s.Serialize(w, obj);
                    w.Close();
                }
            }
            catch (Exception) { }
        }

        public static object DeSerialize(string filepath, Type type)
        {
            try
            {
                object obj;
                using (TextReader r = new StreamReader(filepath))
                {
                    var s = new XmlSerializer(type);
                    obj = s.Deserialize(r);
                    r.Close();
                }
                return obj;
            }
            catch (Exception) { return null; }
        }
    }
}
