﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Awesom_O_Controller
{
    public class AwesomoCoreWatcher
    {
        readonly Process _awesomoCoreProcess = new Process();
        AwesomoCore _awesomoCore;

        public event EventHandler CoreCrashed;
        public bool Running;

        #region External Commands

        public void Start()
        {
            _awesomoCore = AwesomoCoreSettings.GetSettings();
            StartAwesomoCore();
        }

        public void Stop()
        {
            Running = false;
            try { _awesomoCoreProcess.Kill(); }
            catch { }
        }

        public void Restart()
        {
            _awesomoCore = AwesomoCoreSettings.GetSettings();
            Running = false;
            try { _awesomoCoreProcess.Kill(); }
            catch { }
            StartAwesomoCore();
        }

        #endregion

        #region Internal Commands

        private void StartAwesomoCore()
        {
            _awesomoCoreProcess.StartInfo.FileName = _awesomoCore.AwesomoCorePath;
            _awesomoCoreProcess.StartInfo.WorkingDirectory = _awesomoCore.AwesomoCorePath.Remove(_awesomoCore.AwesomoCorePath.LastIndexOf("\\") + 1);
            _awesomoCoreProcess.StartInfo.Arguments = "-d \"" + _awesomoCore.DiabloPath.Remove(_awesomoCore.DiabloPath.LastIndexOf("\\")) + "\" -r " + _awesomoCore.BnetRealm;
            if (_awesomoCore.StealthMode)
                _awesomoCoreProcess.StartInfo.Arguments += " -s";
            if (_awesomoCore.HideAwesomoCore)
                _awesomoCoreProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            try { _awesomoCoreProcess.Start(); }
            catch (Exception e)
            { Log.Error(e.Message); Restart(); return; }
            Running = true;
            new Thread(ErrorWatcher).Start();
        }

        #endregion

        #region Error Watcher

        private void ErrorWatcher()
        {
            while (true)
            {
                try
                {
                    if (!Running)
                    {
                        break;
                    }
                    if (!_awesomoCoreProcess.HasExited && !_awesomoCoreProcess.Responding)
                    {
                        Thread.Sleep(5000);
                        if (!_awesomoCoreProcess.HasExited && !_awesomoCoreProcess.Responding)
                        {
                            Running = false;
                            CoreCrashed(this, new EventArgs());
                            break;
                        }
                    }
                    else if (_awesomoCoreProcess.HasExited)
                    {
                        Running = false;
                        CoreCrashed(this, new EventArgs());
                        break;
                    }
                    /*else if (true)
                        Log.Debug(_awesomoCoreProcess.MainWindowHandle.ToString(), "Core");*/
                }
                catch { }
                Thread.Sleep(1000);
            }
        }

        #endregion
    }
}
