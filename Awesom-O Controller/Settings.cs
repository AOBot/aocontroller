﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Awesom_O_Controller
{
    public class ProfileSettings
    {
        public static void WriteSettings(Profile profile)
        {
            Xml.Serialize(Application.StartupPath + "\\ControllerSettings\\Profiles\\" + profile.Name + ".aoc", typeof(Profile), profile);
        }

        public static Profile GetSettings(string profileName)
        {
            try
            {
                return (Profile)Xml.DeSerialize(Application.StartupPath + "\\ControllerSettings\\Profiles\\" + profileName + ".aoc", typeof(Profile));
            }
            catch (Exception e) 
            { 
                Log.Error(e.Message);
                return new Profile();
            }
        }
    }

    public class AwesomoCoreSettings
    {
        public static bool SettingsExist()
        {
            if (File.Exists(Application.StartupPath + "\\ControllerSettings\\Awesom-O Core Settings.aoc"))
                return true;
            return false;
        }

        public static void WriteSettings(AwesomoCore awesomoCore)
        {
            Xml.Serialize(Application.StartupPath + "\\ControllerSettings\\Awesom-O Core Settings.aoc", typeof(AwesomoCore), awesomoCore);
        }

        public static AwesomoCore GetSettings()
        {
            try
            {
                return (AwesomoCore)Xml.DeSerialize(Application.StartupPath + "\\ControllerSettings\\Awesom-O Core Settings.aoc", typeof(AwesomoCore));
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return new AwesomoCore();
            }
        }
    }
}
