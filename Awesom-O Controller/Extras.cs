﻿using System.Diagnostics;
using System.Linq;
using Microsoft.Win32;

namespace Awesom_O_Controller
{
    class Extras
    {
        public static Process FindDiabloByName(string name)
        {
            return FindWindowByName(name, "game");
        }

        public static Process FindWindowByName(string windowName, string processName)
        {
            return Process.GetProcessesByName(processName).FirstOrDefault(p => p.MainWindowTitle == windowName);
        }

        public static bool DiabloWindowExistsByName(string name)
        {
            return WindowExistsByName(name, "game");
        }

        public static bool WindowExistsByName(string windowName)
        {
            return Process.GetProcesses().Any(p => p.MainWindowTitle.Contains(windowName));
        }

        public static bool WindowExistsByName(string windowName, string processName)
        {
            return Process.GetProcessesByName(processName).Any(p => p.MainWindowTitle.Contains(windowName));
        }

        public static void PreventAct5Bug(string diapath)
        {
            RegistryKey diabloKey = Registry.CurrentUser.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II", true);
            if (diabloKey == null) return;
            var installpath = (string)diabloKey.GetValue("InstallPath");
            if (string.IsNullOrEmpty(installpath)) return;
            if (installpath == diapath) return;
            diabloKey.DeleteValue("InstallPath");
            diabloKey.SetValue("InstallPath", diapath);
        }

        public static void SetRealm()
        {

            RegistryKey battleNetKey = Registry.CurrentUser.OpenSubKey("Software\\Battle.net\\Configuration", true);
            if (battleNetKey == null) return;
            var data = (string[])battleNetKey.GetValue("Diablo II Battle.net gateways");

            if (data == null) return;
            if (((data.Length - 2) % 3) != 0)
            {
                var gateways = new string[17];
                gateways[0] = "1002";
                gateways[1] = "05";
                gateways[2] = "uswest.battle.net";
                gateways[3] = "8";
                gateways[4] = "U.S. West";
                gateways[5] = "useast.battle.net";
                gateways[6] = "6";
                gateways[7] = " U.S. East";
                gateways[8] = "asia.battle.net";
                gateways[9] = "-9";
                gateways[10] = "Asia";
                gateways[11] = "europe.battle.net";
                gateways[12] = "-1";
                gateways[13] = "Europe";
                gateways[14] = "localhost";
                gateways[15] = "0";
                gateways[16] = "Private Realm";
                battleNetKey.SetValue("Diablo II Battle.net gateways", gateways);
            }
            else
            {
                int count = 0;
                int line = 0;
                bool found = false;
                string currentRealm = data[1];

                while (true)
                {
                    if (count <= data.Length)
                    {
                        if ((count + 1) >= data.Length)
                        {
                            break;
                        }
                        line++;
                        if (string.Compare(data[count + 1], "localhost") == 0
                            || string.Compare(data[count + 1], "127.0.0.1") == 0)
                        {
                            found = true;
                            break;
                        }
                    }
                    count++;
                }

                if (found)
                {
                    string proxyRealm = ((((line - 2) / 3) + 1).ToString());
                    if (proxyRealm.Length == 1)
                    {
                        proxyRealm = "0" + proxyRealm;
                    }
                    if (currentRealm == proxyRealm)
                    {
                        Log.Info("No need to change realm");
                    }
                    else
                    {
                        Log.Info("Not the correct realm");
                        Log.Info("Setting the correct realm");
                        data[1] = proxyRealm;
                        battleNetKey.SetValue("Diablo II Battle.net gateways", data);
                        Log.Info("Set to the correct realm");
                    }
                }
                else
                {
                    Log.Info("Proxy realm non-existant");
                    Log.Info("Creating a proxy realm");

                    int length = data.Length;
                    line = 0;
                    var newRealmKey = new string[length + 3];
                    foreach (var s in data)
                    {
                        newRealmKey[line] = s;
                        line++;
                    }
                    newRealmKey[line] = "localhost";
                    newRealmKey[line + 1] = "0";
                    newRealmKey[line + 2] = "Private Realm";
                    battleNetKey.SetValue("Diablo II Battle.net gateways", newRealmKey);
                    SetRealm();

                    Log.Info("Proxy realm created");
                }
            }
        }
    }
}
