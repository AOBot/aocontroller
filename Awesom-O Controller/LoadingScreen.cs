﻿using System;
using System.IO; 
using System.Net;
using System.Windows.Forms;

namespace Awesom_O_Controller
{
    public partial class LoadingScreen : Form
    {
        readonly Timer _timer = new Timer();

        public LoadingScreen()
        {
            InitializeComponent();
        }

        private void LoadingScreenShown(object sender, EventArgs e)
        {
            if (!Directory.Exists("D2Multi"))
                Directory.CreateDirectory("D2Multi");
            if (!File.Exists("D2Multi\\D2M.dll"))
                new WebClient().DownloadFile(new Uri(@"http://tmerr.aobot.org/awesomocontroller/D2Multi/D2M.dll"), "D2Multi\\D2M.dll");
            if (!File.Exists("D2Multi\\D2Multi.exe"))
                new WebClient().DownloadFile(new Uri(@"http://tmerr.aobot.org/awesomocontroller/D2Multi/D2Multi.exe"), "D2Multi\\D2Multi.exe");

            _timer.Interval = 1500;
            _timer.Tick += (TimerTick);
            _timer.Start();
        }

        void TimerTick(object sender, EventArgs e)
        {
            _timer.Stop();
            _timer.Dispose();
            var main = new Main();
            main.FormClosed += MainFormClosed;
            main.Show();
            Hide();
        }

        static void MainFormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
