﻿namespace Awesom_O_Controller
{
    partial class AwesomoCoreConfigurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxAwesomoCorePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxDiabloPath = new System.Windows.Forms.TextBox();
            this.comboBoxBnetRealm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxControllerIP = new System.Windows.Forms.TextBox();
            this.checkBoxStealthMode = new System.Windows.Forms.CheckBox();
            this.checkBoxHideAwesomoCore = new System.Windows.Forms.CheckBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonSaveClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxAwesomoCorePath
            // 
            this.textBoxAwesomoCorePath.Location = new System.Drawing.Point(139, 12);
            this.textBoxAwesomoCorePath.Name = "textBoxAwesomoCorePath";
            this.textBoxAwesomoCorePath.ReadOnly = true;
            this.textBoxAwesomoCorePath.Size = new System.Drawing.Size(195, 20);
            this.textBoxAwesomoCorePath.TabIndex = 0;
            this.textBoxAwesomoCorePath.Click += new System.EventHandler(this.TextBoxAwesomoCorePathClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Awesom-O Core Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Diablo II Path:";
            // 
            // textBoxDiabloPath
            // 
            this.textBoxDiabloPath.Location = new System.Drawing.Point(139, 38);
            this.textBoxDiabloPath.Name = "textBoxDiabloPath";
            this.textBoxDiabloPath.ReadOnly = true;
            this.textBoxDiabloPath.Size = new System.Drawing.Size(195, 20);
            this.textBoxDiabloPath.TabIndex = 2;
            this.textBoxDiabloPath.Click += new System.EventHandler(this.TextBoxDiabloPathClick);
            // 
            // comboBoxBnetRealm
            // 
            this.comboBoxBnetRealm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBnetRealm.FormattingEnabled = true;
            this.comboBoxBnetRealm.Items.AddRange(new object[] {
            "uswest.battle.net",
            "useast.battle.net",
            "asia.battle.net",
            "europe.battle.net"});
            this.comboBoxBnetRealm.Location = new System.Drawing.Point(139, 64);
            this.comboBoxBnetRealm.Name = "comboBoxBnetRealm";
            this.comboBoxBnetRealm.Size = new System.Drawing.Size(195, 21);
            this.comboBoxBnetRealm.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Battle.net Realm:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Controller IP:";
            // 
            // textBoxControllerIP
            // 
            this.textBoxControllerIP.Location = new System.Drawing.Point(139, 91);
            this.textBoxControllerIP.Name = "textBoxControllerIP";
            this.textBoxControllerIP.Size = new System.Drawing.Size(195, 20);
            this.textBoxControllerIP.TabIndex = 7;
            this.textBoxControllerIP.Text = "127.0.0.1";
            // 
            // checkBoxStealthMode
            // 
            this.checkBoxStealthMode.AutoSize = true;
            this.checkBoxStealthMode.Location = new System.Drawing.Point(112, 117);
            this.checkBoxStealthMode.Name = "checkBoxStealthMode";
            this.checkBoxStealthMode.Size = new System.Drawing.Size(89, 17);
            this.checkBoxStealthMode.TabIndex = 8;
            this.checkBoxStealthMode.Text = "Stealth Mode";
            this.checkBoxStealthMode.UseVisualStyleBackColor = true;
            // 
            // checkBoxHideAwesomoCore
            // 
            this.checkBoxHideAwesomoCore.AutoSize = true;
            this.checkBoxHideAwesomoCore.Location = new System.Drawing.Point(207, 117);
            this.checkBoxHideAwesomoCore.Name = "checkBoxHideAwesomoCore";
            this.checkBoxHideAwesomoCore.Size = new System.Drawing.Size(127, 17);
            this.checkBoxHideAwesomoCore.TabIndex = 9;
            this.checkBoxHideAwesomoCore.Text = "Hide Awesom-O Core";
            this.checkBoxHideAwesomoCore.UseVisualStyleBackColor = true;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(176, 140);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(158, 23);
            this.buttonClose.TabIndex = 24;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.ButtonCloseClick);
            // 
            // buttonSaveClose
            // 
            this.buttonSaveClose.Location = new System.Drawing.Point(12, 140);
            this.buttonSaveClose.Name = "buttonSaveClose";
            this.buttonSaveClose.Size = new System.Drawing.Size(158, 23);
            this.buttonSaveClose.TabIndex = 23;
            this.buttonSaveClose.Text = "Save and Close";
            this.buttonSaveClose.UseVisualStyleBackColor = true;
            this.buttonSaveClose.Click += new System.EventHandler(this.ButtonSaveCloseClick);
            // 
            // AwesomoCoreConfigurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 175);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonSaveClose);
            this.Controls.Add(this.checkBoxHideAwesomoCore);
            this.Controls.Add(this.checkBoxStealthMode);
            this.Controls.Add(this.textBoxControllerIP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxBnetRealm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxDiabloPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAwesomoCorePath);
            this.MaximizeBox = false;
            this.Name = "AwesomoCoreConfigurator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Awesom-O Core Configurator";
            this.Shown += new System.EventHandler(this.AwesomoCoreConfigurator_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxAwesomoCorePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxDiabloPath;
        private System.Windows.Forms.ComboBox comboBoxBnetRealm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxControllerIP;
        private System.Windows.Forms.CheckBox checkBoxStealthMode;
        private System.Windows.Forms.CheckBox checkBoxHideAwesomoCore;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonSaveClose;
    }
}