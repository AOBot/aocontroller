﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Awesom_O_Controller
{
    public class Log
    {
        static readonly string LogPath, LogName;
        public static string Message;
        public static event EventHandler MessageLogged;

        static Log()
        {
            LogPath = Application.StartupPath + "\\Logs";
            if (!Directory.Exists(LogPath)) { Directory.CreateDirectory(LogPath); }
            LogName = "\\Awesom-O Controller.log";
            if (File.Exists(LogPath + LogName)) 
            {
                if (!Directory.Exists(LogPath + "\\Archives")) { Directory.CreateDirectory(LogPath + "\\Archives"); }
                File.Copy(LogPath + LogName, LogPath + "\\Archives\\" + String.Format("{0:M-d-yy_H-m-s}", File.GetLastWriteTime(LogPath + LogName)) + ".log");
                File.Delete(LogPath + LogName); 
            }
            Divider(Application.ProductName + " v" + Application.ProductVersion);
        }

        private static void Write(string text)
        {
            try
            {
                Message = text;
                MessageLogged(null, new EventArgs());
                File.AppendAllText(LogPath + LogName, text + Environment.NewLine);
            }
            catch { }
        }

        private static string Time()
        {
            return String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now);
        }

        public static void Debug(string text)
        {
            Write("[" + Time() + "][N/A][Debug]" + text);
        }

        public static void Debug(string text, string profile)
        {
            Write("[" + Time() + "][" + profile + "][Debug]" + text);
        }

        public static void Info(string text)
        {
            Write("[" + Time() + "][N/A][Info]" + text);
        }

        public static void Info(string text, string profile)
        {
            Write("[" + Time() + "][" + profile + "][Info]" + text);
        }

        public static void Error(string text)
        {
            Write("[" + Time() + "][N/A][Error]" + text);
        }

        public static void Error(string text, string profile)
        {
            Write("[" + Time() + "][" + profile + "][Error]" + text);
        }

        public static void Divider()
        {
            Write("======================================================================================"); //86 chars
        }

        public static void Divider(string txt)
        {
            txt = " " + txt.Trim() + " ";
            int half = (86 - txt.Length) / 2;
            string begin = string.Empty;
            for (int i = 1; i <= half; i++) { begin += "="; }
            string end = begin;
            if (((half * 2) + txt.Length) > 86) { end.Remove(half - 1); }
            else if (((half * 2) + txt.Length) < 86) { end += "="; }
            Divider();
            Write(begin + txt + end);
            Divider();
        }
    }
}
