﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Awesom_O_Controller
{
    public partial class AwesomoCoreConfigurator : Form
    {
        AwesomoCore _awesomoCore;

        public AwesomoCoreConfigurator()
        {
            InitializeComponent();
            GetSettings();
        }

        void GetSettings()
        {
            if (AwesomoCoreSettings.SettingsExist())
            {
                _awesomoCore = AwesomoCoreSettings.GetSettings();
                LoadSettings();
            }
            else
            {
                comboBoxBnetRealm.SelectedIndex = 0;
            }
        }

        private void LoadSettings()
        {
            textBoxAwesomoCorePath.Text = _awesomoCore.AwesomoCorePath;
            textBoxDiabloPath.Text = _awesomoCore.DiabloPath;
            comboBoxBnetRealm.Text = _awesomoCore.BnetRealm;
            textBoxControllerIP.Text = _awesomoCore.ControllerIp;
            checkBoxStealthMode.Checked = _awesomoCore.StealthMode;
            checkBoxHideAwesomoCore.Checked = _awesomoCore.HideAwesomoCore;
        }

        private void TextBoxAwesomoCorePathClick(object sender, EventArgs e)
        {
            var fDialog = new OpenFileDialog
                              {
                                  InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                                  Filter = "Awesom-O Core Executable File|Awesom-O.exe",
                                  Title = "Select the Awesom-O Core Executable file"
                              };

            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxAwesomoCorePath.Text = fDialog.FileName;
            }
            fDialog.Dispose();
        }

        private void TextBoxDiabloPathClick(object sender, EventArgs e)
        {
            if (textBoxDiabloPath.Text == string.Empty)
            {
                Microsoft.Win32.RegistryKey blizzardSoftwareKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II", true);

                if (blizzardSoftwareKey == null) return;
                var data = (string)blizzardSoftwareKey.GetValue("InstallPath");

                if (data == null) return;

                if (data.EndsWith("\\"))
                    textBoxDiabloPath.Text = data + "Game.exe";
                else
                    textBoxDiabloPath.Text = data + "\\Game.exe";
            }
            else
            {
                var fDialog = new OpenFileDialog
                                  {
                                      InitialDirectory =
                                          Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                                      Filter = "Diablo II Executable File|Game.exe",
                                      Title = "Select the Diablo II Executable File"
                                  };

                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    textBoxDiabloPath.Text = fDialog.FileName;
                }
                fDialog.Dispose();
            }
        }

        private void ButtonSaveCloseClick(object sender, EventArgs e)
        {
            string temp = Valid();

            if (temp == string.Empty)
            {
                BuildAwesomoCoreSettings();
                AwesomoCoreSettings.WriteSettings(_awesomoCore);
                Close();
            }
            else
                MessageBox.Show(temp);
        }

        private void BuildAwesomoCoreSettings()
        {
            _awesomoCore.AwesomoCorePath = textBoxAwesomoCorePath.Text;
            _awesomoCore.BnetRealm = comboBoxBnetRealm.Text;
            _awesomoCore.ControllerIp = textBoxControllerIP.Text;
            _awesomoCore.DiabloPath = textBoxDiabloPath.Text;
            _awesomoCore.HideAwesomoCore = checkBoxHideAwesomoCore.Checked;
            _awesomoCore.StealthMode = checkBoxStealthMode.Checked;
        }

        private void ButtonCloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private string Valid()
        {
            var errors = new List<string>();

            if (string.IsNullOrEmpty(textBoxAwesomoCorePath.Text))
                errors.Add("Awesom-O Core path is invalid.");
            if (string.IsNullOrEmpty(textBoxDiabloPath.Text))
                errors.Add("Diablo path is invalid.");
            if (string.IsNullOrEmpty(textBoxControllerIP.Text) && ValidIp())
                errors.Add("Controller IP is invalid.");

            if (errors.Count <= 0)
            {
                return string.Empty;
            }
            string temp = string.Empty;
            for (int i = 0; i <= errors.Count - 1; i++)
            {
                if (i == 0)
                    temp += errors[i];
                else
                    temp += "\n" + errors[i];
            }
            return temp;
        }

        private bool ValidIp()
        {
            string ip = textBoxControllerIP.Text;
            int count = ip.Count(t => t == Convert.ToChar("."));
            return count == 3;
        }

        private void AwesomoCoreConfigurator_Shown(object sender, EventArgs e)
        {
            if (textBoxAwesomoCorePath.Text == string.Empty)
                textBoxAwesomoCorePath.Text = Application.StartupPath + "\\Awesom-O.exe";
        }
    }
}
