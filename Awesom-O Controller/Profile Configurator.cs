﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Awesom_O_Controller
{
    public partial class ProfileConfigurator : Form
    {
        Profile _profile;        

        public ProfileConfigurator()
        {
            InitializeComponent();
            comboBoxDifficulty.SelectedIndex = 0;
        }

        public ProfileConfigurator(Profile profile)
        {
            InitializeComponent();
            _profile = profile;
            LoadProfile(profile);
        }

        #region Form Events

        private void TextBoxDiabloPathClick(object sender, EventArgs e)
        {
            if (textBoxDiabloPath.Text == string.Empty)
            {
                Microsoft.Win32.RegistryKey blizzardSoftwareKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II", true);

                if (blizzardSoftwareKey == null) return;
                var data = (string)blizzardSoftwareKey.GetValue("InstallPath");

                if (data == null) return;

                if (data.EndsWith("\\"))
                    textBoxDiabloPath.Text = data + "Game.exe";
                else
                    textBoxDiabloPath.Text = data + "\\Game.exe";
            }
            else
            {
                var fDialog = new OpenFileDialog
                                  {
                                      InitialDirectory =
                                          Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                                      Filter = "Diablo II Executable File|Game.exe",
                                      Title = "Select the Diablo II Executable File"
                                  };

                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    textBoxDiabloPath.Text = fDialog.FileName;
                }
                fDialog.Dispose();
            }
        }

        private void ButtonSaveCloseClick(object sender, EventArgs e)
        {
            string temp = Valid();

            if (temp == string.Empty)
            {
                BuildProfile();
                ProfileSettings.WriteSettings(_profile);
                Close();
            }
            else
                MessageBox.Show(temp);
        }

        private void ButtonCloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void CheckBoxNoGamePasswordCheckedChanged(object sender, EventArgs e)
        {
            textBoxGamePassword.Enabled = !checkBoxNoGamePassword.Checked;
        }

        #endregion

        #region Methods

        private void BuildProfile()
        {
            _profile = new Profile
                           {
                               Name = textBoxProfileName.Text,
                               DiabloPath = textBoxDiabloPath.Text,
                               DiabloOwnerName = textBoxOwnerName.Text,
                               HideDiablo = checkBoxHideDiablo.Checked,
                               MinimizeDiablo = checkBoxMinimizeDiablo.Checked,
                               GamesPerCdKey = Convert.ToInt32(numericUpDownGamesPerCdKey.Value)
                           };
            if (listBoxClassicKeys.Items.Count > 0)
            {
                var keys = listBoxClassicKeys.Items.Cast<string>().ToList();
                _profile.ClassicKeys = keys;
            }
            else
                _profile.ClassicKeys = new List<string>();
            if (listBoxExpansionKeys.Items.Count > 0)
            {
                var keys = listBoxExpansionKeys.Items.Cast<string>().ToList();
                _profile.ExpansionKeys = keys;
            }
            else
                _profile.ExpansionKeys = new List<string>();
            _profile.BnetAccount = textBoxAccount.Text;
            _profile.BnetPassword = textBoxPassword.Text;
            _profile.BnetGameName = textBoxGameName.Text != string.Empty ? textBoxGameName.Text : string.Empty;
            if (textBoxGamePassword.Text != string.Empty && !checkBoxNoGamePassword.Checked)
                _profile.BnetGamePassword = textBoxGamePassword.Text;
            else
                _profile.BnetGamePassword = string.Empty;
            _profile.BnetDifficulty = comboBoxDifficulty.SelectedIndex;
            _profile.BnetCharacterPosition = CharacterPosition();
        }

        string Valid()
        {
            var errors = new List<string>();

            if (string.IsNullOrEmpty(textBoxProfileName.Text))
                errors.Add("Profile name is invalid.");
            if (string.IsNullOrEmpty(textBoxDiabloPath.Text))
                errors.Add("Diablo path is invalid.");
            if (string.IsNullOrEmpty(textBoxOwnerName.Text))
                errors.Add("Owner name is invalid.");
            if (string.IsNullOrEmpty(textBoxAccount.Text))
                errors.Add("Battle.net account name is invalid.");
            if (string.IsNullOrEmpty(textBoxPassword.Text))
                errors.Add("Battle.net account password is invalid.");

            if (errors.Count > 0)
            {
                string temp = string.Empty;
                for (int i = 0; i <= errors.Count - 1; i++)
                {
                    if (i == 0)
                        temp += errors[i];
                    else
                        temp += "\n" + errors[i];
                }
                return temp;
            }
            return string.Empty;
        }

        void LoadProfile(Profile profile)
        {
            textBoxProfileName.Text = profile.Name;
            textBoxDiabloPath.Text = profile.DiabloPath;
            textBoxOwnerName.Text = profile.DiabloOwnerName;
            checkBoxHideDiablo.Checked = profile.HideDiablo;
            checkBoxMinimizeDiablo.Checked = profile.MinimizeDiablo;
            numericUpDownGamesPerCdKey.Value = profile.GamesPerCdKey;
            if (profile.ClassicKeys.Count > 0)
                listBoxClassicKeys.Items.AddRange(profile.ClassicKeys.ToArray());
            if (profile.ExpansionKeys.Count > 0)
                listBoxExpansionKeys.Items.AddRange(profile.ExpansionKeys.ToArray());
            textBoxAccount.Text = profile.BnetAccount;
            textBoxPassword.Text = profile.BnetPassword;
            textBoxGameName.Text = profile.BnetGameName;
            textBoxGamePassword.Text = profile.BnetGamePassword;
            comboBoxDifficulty.SelectedIndex = profile.BnetDifficulty;
            CharacterPosition(profile);
        }

        private int CharacterPosition()
        {
            if (radioButtonCharPos1.Checked)
                return 0;
            if (radioButtonCharPos2.Checked)
                return 1;
            if (radioButtonCharPos3.Checked)
                return 2;
            if (radioButtonCharPos4.Checked)
                return 3;
            if (radioButtonCharPos5.Checked)
                return 4;
            if (radioButtonCharPos6.Checked)
                return 5;
            if (radioButtonCharPos7.Checked)
                return 6;
            if (radioButtonCharPos8.Checked)
                return 7;
            return 0;
        }

        private void CharacterPosition(Profile profile)
        {
            switch (profile.BnetCharacterPosition)
            {
                case 0: radioButtonCharPos1.Checked = true; return;
                case 1: radioButtonCharPos2.Checked = true; return;
                case 2: radioButtonCharPos3.Checked = true; return;
                case 3: radioButtonCharPos4.Checked = true; return;
                case 4: radioButtonCharPos5.Checked = true; return;
                case 5: radioButtonCharPos6.Checked = true; return;
                case 6: radioButtonCharPos7.Checked = true; return;
                case 7: radioButtonCharPos8.Checked = true; return;
            }
            radioButtonCharPos1.Checked = true;
        }

        #endregion
    }
}
