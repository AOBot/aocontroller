﻿using System.Collections.Generic;

namespace Awesom_O_Controller
{
    public struct AwesomoCore
    {
        public string AwesomoCorePath, DiabloPath, BnetRealm, ControllerIp;
        public bool StealthMode, HideAwesomoCore;

        public AwesomoCore(string awesomoCorePath, string diabloPath, string bnetRealm, string controllerIp, bool stealthMode, bool hideAwesomoCore)
        {
            AwesomoCorePath = awesomoCorePath;
            DiabloPath = diabloPath;
            BnetRealm = bnetRealm;
            ControllerIp = controllerIp;
            StealthMode = stealthMode;
            HideAwesomoCore = hideAwesomoCore;
        }
    }

    public struct Profile
    {
        public bool  HideDiablo, MinimizeDiablo;
        public int GamesPerCdKey, BnetDifficulty, BnetCharacterPosition;
        public string Name, DiabloPath, DiabloOwnerName, BnetAccount, BnetPassword, BnetGameName, BnetGamePassword;

        public List<string> ClassicKeys, ExpansionKeys;

        public Profile(string name, string diabloPath, string diabloOwnerName, bool hideDiablo, bool minimizeDiablo, 
            int gamesPerCdKey, string bnetAccount, string bnetPassword, string bnetGameName, string bnetGamePassword, int bnetDifficulty, 
            int bnetCharacterPosition, List<string> classicKeys, List<string> expansionKeys)
        {
            Name = name;
            DiabloPath = diabloPath;
            DiabloOwnerName = diabloOwnerName;
            HideDiablo = hideDiablo;
            MinimizeDiablo = minimizeDiablo;
            GamesPerCdKey = gamesPerCdKey;
            BnetAccount = bnetAccount;
            BnetPassword = bnetPassword;
            BnetGameName = bnetGameName;
            BnetGamePassword = bnetGamePassword;
            BnetDifficulty = bnetDifficulty;
            BnetCharacterPosition = bnetCharacterPosition;

            ClassicKeys = classicKeys;
            ExpansionKeys = expansionKeys;
        }
    }
}
