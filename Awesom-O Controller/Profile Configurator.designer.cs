﻿namespace Awesom_O_Controller
{
    partial class ProfileConfigurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfileConfigurator));
            this.buttonSaveClose = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxAccount = new System.Windows.Forms.TextBox();
            this.textBoxOwnerName = new System.Windows.Forms.TextBox();
            this.textBoxDiabloPath = new System.Windows.Forms.TextBox();
            this.textBoxProfileName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxDiabloSettings = new System.Windows.Forms.GroupBox();
            this.buttonAddExpansionKey = new System.Windows.Forms.Button();
            this.listBoxExpansionKeys = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxExpansionKey = new System.Windows.Forms.TextBox();
            this.buttonAddClassicKey = new System.Windows.Forms.Button();
            this.listBoxClassicKeys = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxClassicKey = new System.Windows.Forms.TextBox();
            this.numericUpDownGamesPerCdKey = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxMinimizeDiablo = new System.Windows.Forms.CheckBox();
            this.checkBoxHideDiablo = new System.Windows.Forms.CheckBox();
            this.groupBoxBattleNetSettings = new System.Windows.Forms.GroupBox();
            this.checkBoxNoGamePassword = new System.Windows.Forms.CheckBox();
            this.radioButtonCharPos8 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos7 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos6 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos5 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos4 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos3 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos2 = new System.Windows.Forms.RadioButton();
            this.radioButtonCharPos1 = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxDifficulty = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxGamePassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxGameName = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageBasic = new System.Windows.Forms.TabPage();
            this.tabPageAdvanced = new System.Windows.Forms.TabPage();
            this.groupBoxDiabloSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamesPerCdKey)).BeginInit();
            this.groupBoxBattleNetSettings.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageBasic.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSaveClose
            // 
            this.buttonSaveClose.Location = new System.Drawing.Point(349, 354);
            this.buttonSaveClose.Name = "buttonSaveClose";
            this.buttonSaveClose.Size = new System.Drawing.Size(162, 23);
            this.buttonSaveClose.TabIndex = 1;
            this.buttonSaveClose.Text = "Save and Close";
            this.buttonSaveClose.UseVisualStyleBackColor = true;
            this.buttonSaveClose.Click += new System.EventHandler(this.ButtonSaveCloseClick);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(117, 45);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '•';
            this.textBoxPassword.Size = new System.Drawing.Size(198, 20);
            this.textBoxPassword.TabIndex = 19;
            // 
            // textBoxAccount
            // 
            this.textBoxAccount.Location = new System.Drawing.Point(117, 19);
            this.textBoxAccount.Name = "textBoxAccount";
            this.textBoxAccount.Size = new System.Drawing.Size(198, 20);
            this.textBoxAccount.TabIndex = 18;
            // 
            // textBoxOwnerName
            // 
            this.textBoxOwnerName.Location = new System.Drawing.Point(117, 45);
            this.textBoxOwnerName.Name = "textBoxOwnerName";
            this.textBoxOwnerName.Size = new System.Drawing.Size(198, 20);
            this.textBoxOwnerName.TabIndex = 17;
            // 
            // textBoxDiabloPath
            // 
            this.textBoxDiabloPath.Location = new System.Drawing.Point(117, 19);
            this.textBoxDiabloPath.Name = "textBoxDiabloPath";
            this.textBoxDiabloPath.ReadOnly = true;
            this.textBoxDiabloPath.Size = new System.Drawing.Size(198, 20);
            this.textBoxDiabloPath.TabIndex = 16;
            this.textBoxDiabloPath.Click += new System.EventHandler(this.TextBoxDiabloPathClick);
            // 
            // textBoxProfileName
            // 
            this.textBoxProfileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxProfileName.Location = new System.Drawing.Point(116, 12);
            this.textBoxProfileName.Name = "textBoxProfileName";
            this.textBoxProfileName.Size = new System.Drawing.Size(198, 26);
            this.textBoxProfileName.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Account:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Owner Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Path:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Profile Name:";
            // 
            // groupBoxDiabloSettings
            // 
            this.groupBoxDiabloSettings.Controls.Add(this.buttonAddExpansionKey);
            this.groupBoxDiabloSettings.Controls.Add(this.listBoxExpansionKeys);
            this.groupBoxDiabloSettings.Controls.Add(this.label8);
            this.groupBoxDiabloSettings.Controls.Add(this.textBoxExpansionKey);
            this.groupBoxDiabloSettings.Controls.Add(this.buttonAddClassicKey);
            this.groupBoxDiabloSettings.Controls.Add(this.listBoxClassicKeys);
            this.groupBoxDiabloSettings.Controls.Add(this.label7);
            this.groupBoxDiabloSettings.Controls.Add(this.textBoxClassicKey);
            this.groupBoxDiabloSettings.Controls.Add(this.numericUpDownGamesPerCdKey);
            this.groupBoxDiabloSettings.Controls.Add(this.label6);
            this.groupBoxDiabloSettings.Controls.Add(this.checkBoxMinimizeDiablo);
            this.groupBoxDiabloSettings.Controls.Add(this.checkBoxHideDiablo);
            this.groupBoxDiabloSettings.Controls.Add(this.textBoxOwnerName);
            this.groupBoxDiabloSettings.Controls.Add(this.textBoxDiabloPath);
            this.groupBoxDiabloSettings.Controls.Add(this.label2);
            this.groupBoxDiabloSettings.Controls.Add(this.label3);
            this.groupBoxDiabloSettings.Location = new System.Drawing.Point(6, 6);
            this.groupBoxDiabloSettings.Name = "groupBoxDiabloSettings";
            this.groupBoxDiabloSettings.Size = new System.Drawing.Size(321, 270);
            this.groupBoxDiabloSettings.TabIndex = 20;
            this.groupBoxDiabloSettings.TabStop = false;
            this.groupBoxDiabloSettings.Text = "Diablo II Settings";
            // 
            // buttonAddExpansionKey
            // 
            this.buttonAddExpansionKey.Enabled = false;
            this.buttonAddExpansionKey.Location = new System.Drawing.Point(261, 195);
            this.buttonAddExpansionKey.Name = "buttonAddExpansionKey";
            this.buttonAddExpansionKey.Size = new System.Drawing.Size(54, 20);
            this.buttonAddExpansionKey.TabIndex = 29;
            this.buttonAddExpansionKey.Text = "Add";
            this.buttonAddExpansionKey.UseVisualStyleBackColor = true;
            // 
            // listBoxExpansionKeys
            // 
            this.listBoxExpansionKeys.FormattingEnabled = true;
            this.listBoxExpansionKeys.Location = new System.Drawing.Point(117, 221);
            this.listBoxExpansionKeys.Name = "listBoxExpansionKeys";
            this.listBoxExpansionKeys.Size = new System.Drawing.Size(138, 43);
            this.listBoxExpansionKeys.TabIndex = 28;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Expansion Key:";
            // 
            // textBoxExpansionKey
            // 
            this.textBoxExpansionKey.Enabled = false;
            this.textBoxExpansionKey.Location = new System.Drawing.Point(117, 195);
            this.textBoxExpansionKey.MaxLength = 16;
            this.textBoxExpansionKey.Name = "textBoxExpansionKey";
            this.textBoxExpansionKey.Size = new System.Drawing.Size(138, 20);
            this.textBoxExpansionKey.TabIndex = 26;
            // 
            // buttonAddClassicKey
            // 
            this.buttonAddClassicKey.Enabled = false;
            this.buttonAddClassicKey.Location = new System.Drawing.Point(261, 120);
            this.buttonAddClassicKey.Name = "buttonAddClassicKey";
            this.buttonAddClassicKey.Size = new System.Drawing.Size(54, 20);
            this.buttonAddClassicKey.TabIndex = 25;
            this.buttonAddClassicKey.Text = "Add";
            this.buttonAddClassicKey.UseVisualStyleBackColor = true;
            // 
            // listBoxClassicKeys
            // 
            this.listBoxClassicKeys.FormattingEnabled = true;
            this.listBoxClassicKeys.Location = new System.Drawing.Point(117, 146);
            this.listBoxClassicKeys.Name = "listBoxClassicKeys";
            this.listBoxClassicKeys.Size = new System.Drawing.Size(138, 43);
            this.listBoxClassicKeys.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Classic Key:";
            // 
            // textBoxClassicKey
            // 
            this.textBoxClassicKey.Enabled = false;
            this.textBoxClassicKey.Location = new System.Drawing.Point(117, 120);
            this.textBoxClassicKey.MaxLength = 16;
            this.textBoxClassicKey.Name = "textBoxClassicKey";
            this.textBoxClassicKey.Size = new System.Drawing.Size(138, 20);
            this.textBoxClassicKey.TabIndex = 22;
            // 
            // numericUpDownGamesPerCdKey
            // 
            this.numericUpDownGamesPerCdKey.Enabled = false;
            this.numericUpDownGamesPerCdKey.Location = new System.Drawing.Point(117, 94);
            this.numericUpDownGamesPerCdKey.Name = "numericUpDownGamesPerCdKey";
            this.numericUpDownGamesPerCdKey.Size = new System.Drawing.Size(198, 20);
            this.numericUpDownGamesPerCdKey.TabIndex = 21;
            this.numericUpDownGamesPerCdKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Games/CD-Key:";
            // 
            // checkBoxMinimizeDiablo
            // 
            this.checkBoxMinimizeDiablo.AutoSize = true;
            this.checkBoxMinimizeDiablo.Enabled = false;
            this.checkBoxMinimizeDiablo.Location = new System.Drawing.Point(171, 71);
            this.checkBoxMinimizeDiablo.Name = "checkBoxMinimizeDiablo";
            this.checkBoxMinimizeDiablo.Size = new System.Drawing.Size(66, 17);
            this.checkBoxMinimizeDiablo.TabIndex = 19;
            this.checkBoxMinimizeDiablo.Text = "Minimize";
            this.checkBoxMinimizeDiablo.UseVisualStyleBackColor = true;
            // 
            // checkBoxHideDiablo
            // 
            this.checkBoxHideDiablo.AutoSize = true;
            this.checkBoxHideDiablo.Enabled = false;
            this.checkBoxHideDiablo.Location = new System.Drawing.Point(117, 71);
            this.checkBoxHideDiablo.Name = "checkBoxHideDiablo";
            this.checkBoxHideDiablo.Size = new System.Drawing.Size(48, 17);
            this.checkBoxHideDiablo.TabIndex = 18;
            this.checkBoxHideDiablo.Text = "Hide";
            this.checkBoxHideDiablo.UseVisualStyleBackColor = true;
            // 
            // groupBoxBattleNetSettings
            // 
            this.groupBoxBattleNetSettings.Controls.Add(this.checkBoxNoGamePassword);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos8);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos7);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos6);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos5);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos4);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos3);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos2);
            this.groupBoxBattleNetSettings.Controls.Add(this.radioButtonCharPos1);
            this.groupBoxBattleNetSettings.Controls.Add(this.label13);
            this.groupBoxBattleNetSettings.Controls.Add(this.comboBoxDifficulty);
            this.groupBoxBattleNetSettings.Controls.Add(this.label12);
            this.groupBoxBattleNetSettings.Controls.Add(this.label10);
            this.groupBoxBattleNetSettings.Controls.Add(this.textBoxGamePassword);
            this.groupBoxBattleNetSettings.Controls.Add(this.label9);
            this.groupBoxBattleNetSettings.Controls.Add(this.textBoxGameName);
            this.groupBoxBattleNetSettings.Controls.Add(this.label4);
            this.groupBoxBattleNetSettings.Controls.Add(this.label5);
            this.groupBoxBattleNetSettings.Controls.Add(this.textBoxPassword);
            this.groupBoxBattleNetSettings.Controls.Add(this.textBoxAccount);
            this.groupBoxBattleNetSettings.Location = new System.Drawing.Point(333, 6);
            this.groupBoxBattleNetSettings.Name = "groupBoxBattleNetSettings";
            this.groupBoxBattleNetSettings.Size = new System.Drawing.Size(321, 270);
            this.groupBoxBattleNetSettings.TabIndex = 21;
            this.groupBoxBattleNetSettings.TabStop = false;
            this.groupBoxBattleNetSettings.Text = "Battle.net Settings";
            // 
            // checkBoxNoGamePassword
            // 
            this.checkBoxNoGamePassword.AutoSize = true;
            this.checkBoxNoGamePassword.Enabled = false;
            this.checkBoxNoGamePassword.Location = new System.Drawing.Point(263, 99);
            this.checkBoxNoGamePassword.Name = "checkBoxNoGamePassword";
            this.checkBoxNoGamePassword.Size = new System.Drawing.Size(52, 17);
            this.checkBoxNoGamePassword.TabIndex = 30;
            this.checkBoxNoGamePassword.Text = "None";
            this.checkBoxNoGamePassword.UseVisualStyleBackColor = true;
            this.checkBoxNoGamePassword.CheckedChanged += new System.EventHandler(this.CheckBoxNoGamePasswordCheckedChanged);
            // 
            // radioButtonCharPos8
            // 
            this.radioButtonCharPos8.AutoSize = true;
            this.radioButtonCharPos8.Location = new System.Drawing.Point(154, 219);
            this.radioButtonCharPos8.Name = "radioButtonCharPos8";
            this.radioButtonCharPos8.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos8.TabIndex = 36;
            this.radioButtonCharPos8.Text = "8";
            this.radioButtonCharPos8.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos7
            // 
            this.radioButtonCharPos7.AutoSize = true;
            this.radioButtonCharPos7.Location = new System.Drawing.Point(117, 219);
            this.radioButtonCharPos7.Name = "radioButtonCharPos7";
            this.radioButtonCharPos7.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos7.TabIndex = 35;
            this.radioButtonCharPos7.Text = "7";
            this.radioButtonCharPos7.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos6
            // 
            this.radioButtonCharPos6.AutoSize = true;
            this.radioButtonCharPos6.Location = new System.Drawing.Point(154, 196);
            this.radioButtonCharPos6.Name = "radioButtonCharPos6";
            this.radioButtonCharPos6.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos6.TabIndex = 34;
            this.radioButtonCharPos6.Text = "6";
            this.radioButtonCharPos6.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos5
            // 
            this.radioButtonCharPos5.AutoSize = true;
            this.radioButtonCharPos5.Location = new System.Drawing.Point(117, 196);
            this.radioButtonCharPos5.Name = "radioButtonCharPos5";
            this.radioButtonCharPos5.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos5.TabIndex = 33;
            this.radioButtonCharPos5.Text = "5";
            this.radioButtonCharPos5.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos4
            // 
            this.radioButtonCharPos4.AutoSize = true;
            this.radioButtonCharPos4.Location = new System.Drawing.Point(154, 173);
            this.radioButtonCharPos4.Name = "radioButtonCharPos4";
            this.radioButtonCharPos4.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos4.TabIndex = 32;
            this.radioButtonCharPos4.Text = "4";
            this.radioButtonCharPos4.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos3
            // 
            this.radioButtonCharPos3.AutoSize = true;
            this.radioButtonCharPos3.Location = new System.Drawing.Point(117, 173);
            this.radioButtonCharPos3.Name = "radioButtonCharPos3";
            this.radioButtonCharPos3.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos3.TabIndex = 31;
            this.radioButtonCharPos3.Text = "3";
            this.radioButtonCharPos3.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos2
            // 
            this.radioButtonCharPos2.AutoSize = true;
            this.radioButtonCharPos2.Location = new System.Drawing.Point(154, 150);
            this.radioButtonCharPos2.Name = "radioButtonCharPos2";
            this.radioButtonCharPos2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos2.TabIndex = 30;
            this.radioButtonCharPos2.Text = "2";
            this.radioButtonCharPos2.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharPos1
            // 
            this.radioButtonCharPos1.AutoSize = true;
            this.radioButtonCharPos1.Checked = true;
            this.radioButtonCharPos1.Location = new System.Drawing.Point(117, 150);
            this.radioButtonCharPos1.Name = "radioButtonCharPos1";
            this.radioButtonCharPos1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCharPos1.TabIndex = 29;
            this.radioButtonCharPos1.TabStop = true;
            this.radioButtonCharPos1.Text = "1";
            this.radioButtonCharPos1.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 152);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Character Position:";
            // 
            // comboBoxDifficulty
            // 
            this.comboBoxDifficulty.DisplayMember = "U.S. West";
            this.comboBoxDifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDifficulty.FormattingEnabled = true;
            this.comboBoxDifficulty.Items.AddRange(new object[] {
            "Hell",
            "Nightmare",
            "Normal"});
            this.comboBoxDifficulty.Location = new System.Drawing.Point(117, 123);
            this.comboBoxDifficulty.Name = "comboBoxDifficulty";
            this.comboBoxDifficulty.Size = new System.Drawing.Size(198, 21);
            this.comboBoxDifficulty.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Difficulty:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Game Password:";
            // 
            // textBoxGamePassword
            // 
            this.textBoxGamePassword.Enabled = false;
            this.textBoxGamePassword.Location = new System.Drawing.Point(117, 97);
            this.textBoxGamePassword.Name = "textBoxGamePassword";
            this.textBoxGamePassword.Size = new System.Drawing.Size(140, 20);
            this.textBoxGamePassword.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Game Name:";
            // 
            // textBoxGameName
            // 
            this.textBoxGameName.Enabled = false;
            this.textBoxGameName.Location = new System.Drawing.Point(117, 71);
            this.textBoxGameName.Name = "textBoxGameName";
            this.textBoxGameName.Size = new System.Drawing.Size(198, 20);
            this.textBoxGameName.TabIndex = 21;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(518, 354);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(162, 23);
            this.buttonClose.TabIndex = 22;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.ButtonCloseClick);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageBasic);
            this.tabControl.Controls.Add(this.tabPageAdvanced);
            this.tabControl.Location = new System.Drawing.Point(12, 44);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(668, 308);
            this.tabControl.TabIndex = 23;
            // 
            // tabPageBasic
            // 
            this.tabPageBasic.Controls.Add(this.groupBoxDiabloSettings);
            this.tabPageBasic.Controls.Add(this.groupBoxBattleNetSettings);
            this.tabPageBasic.Location = new System.Drawing.Point(4, 22);
            this.tabPageBasic.Name = "tabPageBasic";
            this.tabPageBasic.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBasic.Size = new System.Drawing.Size(660, 282);
            this.tabPageBasic.TabIndex = 0;
            this.tabPageBasic.Text = "Basic";
            this.tabPageBasic.UseVisualStyleBackColor = true;
            // 
            // tabPageAdvanced
            // 
            this.tabPageAdvanced.Location = new System.Drawing.Point(4, 22);
            this.tabPageAdvanced.Name = "tabPageAdvanced";
            this.tabPageAdvanced.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdvanced.Size = new System.Drawing.Size(660, 282);
            this.tabPageAdvanced.TabIndex = 1;
            this.tabPageAdvanced.Text = "Advanced";
            this.tabPageAdvanced.UseVisualStyleBackColor = true;
            // 
            // ProfileConfigurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 389);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.textBoxProfileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSaveClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProfileConfigurator";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Profile Configurator";
            this.groupBoxDiabloSettings.ResumeLayout(false);
            this.groupBoxDiabloSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamesPerCdKey)).EndInit();
            this.groupBoxBattleNetSettings.ResumeLayout(false);
            this.groupBoxBattleNetSettings.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageBasic.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSaveClose;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxAccount;
        private System.Windows.Forms.TextBox textBoxOwnerName;
        private System.Windows.Forms.TextBox textBoxDiabloPath;
        private System.Windows.Forms.TextBox textBoxProfileName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxDiabloSettings;
        private System.Windows.Forms.CheckBox checkBoxMinimizeDiablo;
        private System.Windows.Forms.CheckBox checkBoxHideDiablo;
        private System.Windows.Forms.GroupBox groupBoxBattleNetSettings;
        private System.Windows.Forms.ListBox listBoxClassicKeys;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxClassicKey;
        private System.Windows.Forms.NumericUpDown numericUpDownGamesPerCdKey;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonAddClassicKey;
        private System.Windows.Forms.Button buttonAddExpansionKey;
        private System.Windows.Forms.ListBox listBoxExpansionKeys;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxExpansionKey;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxGamePassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxGameName;
        private System.Windows.Forms.ComboBox comboBoxDifficulty;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton radioButtonCharPos8;
        private System.Windows.Forms.RadioButton radioButtonCharPos7;
        private System.Windows.Forms.RadioButton radioButtonCharPos6;
        private System.Windows.Forms.RadioButton radioButtonCharPos5;
        private System.Windows.Forms.RadioButton radioButtonCharPos4;
        private System.Windows.Forms.RadioButton radioButtonCharPos3;
        private System.Windows.Forms.RadioButton radioButtonCharPos2;
        private System.Windows.Forms.RadioButton radioButtonCharPos1;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageBasic;
        private System.Windows.Forms.TabPage tabPageAdvanced;
        private System.Windows.Forms.CheckBox checkBoxNoGamePassword;
    }
}